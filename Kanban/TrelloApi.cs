﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace Kanban
{
    public class TrelloApi
    {
        private string _appKey;
        private string _token;
        public TrelloApi(string appKey, string token)
        {
            this._appKey = appKey;
            this._token = token;
        }

        private void addTokenQs(WebClient client)
        {
            client.QueryString.Add("key", _appKey);
            client.QueryString.Add("token", _token);
        }

        public List<TrelloCard> GetCards(string listId)
        {
            var baseUrl = "https://api.trello.com/1/lists/";
            var url = baseUrl + listId + "/cards";
            using (WebClient client = new WebClient())
            {
                addTokenQs(client);
                var json = client.DownloadString(url);
                return JsonConvert.DeserializeObject<List<TrelloCard>>(json);
            }
        }

        public void MoveCard(string sourceCardId, string destinationListId, string destinationBoardId = null)
        {
            var baseUrl = "https://api.trello.com/1/cards/";
            var url = baseUrl + sourceCardId + "/";
            using (WebClient client = new WebClient())
            {
                addTokenQs(client);
                client.QueryString.Add("idList", destinationListId);
                if (destinationBoardId != null)
                {
                    client.QueryString.Add("idBoard", destinationBoardId);
                }
                var json = client.UploadString(url, "Put", "");
            }
        }
    }
}
