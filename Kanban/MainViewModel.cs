﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kanban
{
    public class MainViewModel
    {
        private string _appKey = "eed1d500e881bfd6405c848fbb77bfe3";
        private string _token = "fdedc2f50e957c251a25b8656f8b2546f723e2f85c2fb4ce734388f7bcb2b107"; // todo: move this stuff to config if any further aciton would be required
        private string _AboardId = "58da5bf3bd90d11cbe7469f6";
        private string _BboardId = "58da5bf875fec6ce85bf4af0";
        private string _A_Y_id = "58da5c118cc0baae838bb4b6";
        private string _A_Z_id = "58da5c156b207e7b90ba29e8";
        private string _B_F_id = "58da5c05072c983802173012";
        private string _B_H_id = "58da5c08032f9677495ef349";
        private string _B_J_id = "58da5c0a02f192d92fa48a94";
        TrelloApi api;
        private ICommand _command;
        public ICommand BHtoBJCommand
        {
            get
            {
                return _command ?? (_command = new CommandHandler(() => BHtoBJ(), true));
            }
        }
        public MainViewModel()
        {
            api = new TrelloApi(_appKey, _token);
            _startWorkerA_Y_to_B_F();
            _startWorkerB_J_to_A_Z();
        }

        private void _startWorkerB_J_to_A_Z()
        {
            var timer = new System.Threading.Timer((e) =>
            {
                var BJCards = api.GetCards(_B_J_id);
                BJCards.ForEach(c => api.MoveCard(c.id, _A_Z_id, _AboardId));
            },null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
            
        }

        private void _startWorkerA_Y_to_B_F()
        {
            var timer = new System.Threading.Timer((e) =>
            {
                var AYCards = api.GetCards(_A_Y_id);
                AYCards.ForEach(c => api.MoveCard(c.id, _B_F_id, _BboardId));
            }, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
        }

        private void BHtoBJ()
        {
            var BHCards = api.GetCards(_B_H_id);
            BHCards.ForEach(c => api.MoveCard(c.id, _B_J_id));
        }
    }

    public class CommandHandler : ICommand
    {
        private Action _action;
        private bool _canExecute;
        public CommandHandler(Action action, bool canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _action();
        }
    }
}
